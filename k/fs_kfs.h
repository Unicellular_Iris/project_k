#ifndef FS_KFS_H_
#define FS_KFS_H_

#include <k/kfs.h>

#define KFS_MAX_FD 256

typedef struct s_opened_file
{
  struct kfs_inode *inode;
  u32 offset;
  u32 block;
} t_opened_file;

typedef struct s_available_file
{
  char filename[KFS_FNAME_SZ];
  u32 size;
} t_available_file;

#endif /* FS_KFS_H_ */
