/*
 * Copyright (c) LSE
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY LSE AS IS AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL LSE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <k/kstd.h>
#include "multiboot.h"

#include "io.h"
#include "gdt.h"
#include "idt.h"
#include "hw_pit.h"
#include "keyboard.h"
#include "elf.h"
#include <k/kfs.h>

void init_gdt()
{
  add_gdt_entry(0, 0, 0, 0, 0); // Null descriptor
  add_gdt_entry(1, 0, 0xFFFFFFFF, 0x9A, 0xCF); // Kernel code
  add_gdt_entry(2, 0, 0xFFFFFFFF, 0x92, 0xCF); // Kernel data
  add_gdt_entry(3, 0, 0xFFFFFFFF, 0xFA, 0xCF); // User code
  add_gdt_entry(4, 0, 0xFFFFFFFF, 0xF2, 0xCF); // User data
  
  gdt_info.limit = (sizeof(t_gdt) * GDT_SIZE) - 1;
  gdt_info.base = &gdt;
  add_tss();
  load_gdt();

  kwrite("GDT Loaded!\n\r", 13);
  kprint("%p\n\r", &gdt);
}

void memory(multiboot_info_t *info){
	memory_map_t* mmap = (memory_map_t *)info->mmap_addr;
	kprint("%p\n\r", ((module_t *)info->mods_addr)[0].mod_end);

	kprint("%d\n\r", sizeof(memory_map_t));

	while(mmap < info->mmap_addr + info->mmap_length) {
		kprint(
				"%p : %d\n\r"
				" -> %p %d\n\r"
				" -> %p\n\r"
				"\n\r",
				mmap, mmap->size,
				mmap->base_addr_low, mmap->length_low,
				mmap->type
			);
		mmap = (memory_map_t*) ( (unsigned int)mmap + mmap->size + sizeof(mmap->size) );
	}
}

Elf32_Ehdr elf;

void (*g_func)(void);

void rom_load(multiboot_info_t *info){
  char buf[512];
  int fd = kfs_open(&(((char *)info->cmdline)[1]), 'r');
  Elf32_Phdr elf_p;

  if (kfs_read(fd, &elf, sizeof(Elf32_Ehdr)) == sizeof(Elf32_Ehdr))
  {
	for(int i = 0; i < elf.e_phnum; i++)
	{
		kfs_seek(fd, elf.e_phoff + (i * sizeof(Elf32_Phdr)), SEEK_SET);
		kfs_read(fd, &elf_p, sizeof(Elf32_Phdr));
		
		u32 memsize = elf_p.p_memsz;
		u32 filesize = elf_p.p_filesz;
		u32 mempos = elf_p.p_vaddr;
		u32 filepos = elf_p.p_offset;
		kprint(
				"%d\n\r"
				" -> memsize %d\n\r"
				" -> filesize %d\n\r",
				i,
				memsize,
				filesize);

		
		kfs_seek(fd, filepos, SEEK_SET);
		u32 i = 0;
		while (i < filesize){
			if (filesize - i < 512){
				kfs_read(fd, &buf, filesize - i);
				memcpy(mempos, &buf, filesize);
			}
			else{
				kfs_read(fd, &buf, 512);
				memcpy(mempos, &buf, 512);
			}
			i += 512;
		}
		memset(mempos + filesize, 0, memsize - filesize);
	}
  }
}

void exec(){
	kprint("\n\r");
	g_func();
}

void k_main(unsigned long magic, multiboot_info_t *info)
{
  (void)magic;
  (void)info;

  init_keymap();
  init_serial();
  init_gdt();
  init_idt();
  init_pit(PIT_CHANNEL_0, PIT_ACCESS_LOHI, PIT_MODE_2, 8000);
  init_kfs(info);
  print_time();
  rom_load(info);
  load_tss();
  
// memory(info);

//  int res;
//  asm("int $0x80" : "=a"(res) : "a"(SYSCALL_SBRK), "b"(0));


  kprint(
		  "Execute ROM\n\r"\
		  "================\n\r"
		  "\n\r");

  g_func = ((void (*)(void))(elf.e_entry));
  tss_entry.eip = g_func;
  asm volatile("mov %0, %%ds": : "a"(0x20 | 0x3));
  asm volatile("mov %0, %%es": : "a"(0x20 | 0x3));
  asm volatile("mov %0, %%cs": : "a"(0x18 | 0x3));
  asm volatile("movl %0, %%eip": : "i"(g_func));

  __asm__ volatile("iret");

  g_func();
//  char star[4] = "|/-\\";
//  char *fb = (void *)0xb8000;
//  u8 keycode;
//  char c;
//  for (unsigned i = 0; ; ) {
//    *(fb+2) = star[i++ % 4];
//
//    keycode = pop_key();
//    c = keymap[keycode];
//
//    if (keycode != BUFFER_DEFAULT)
//      kwrite(&c, 1);
//  }
  
  for (;;)
    {
      asm volatile ("hlt");
    }
}
