#ifndef IO_H_
#define IO_H_

#include <k/types.h>

#define PORT_COM1 0x3f8
#define PORT_COM2 0x2F8
#define PORT_COM3 0x3E8
#define PORT_COM4 0x2E8

#define UART_IER_DLH	1
#define UART_IIR	2
#define UART_LCR	3
#define UART_MCR	4
#define UART_LSR	5
#define UART_MSR	6
#define UART_SR		7

static inline void outb(u16 port, u8 val)
{
  asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
}

static inline void outw(u16 port, u16 val)
{
  asm volatile ( "outw %0, %1" : : "a"(val), "Nd"(port) );
}

static inline void outl(u16 port, u32 val)
{
  asm volatile ( "outl %0, %1" : : "a"(val), "Nd"(port) );
}

static inline u8 inb(u16 port)
{
  u8 ret;
  asm volatile ( "inb %1, %0"
		 : "=a"(ret)
		 : "Nd"(port) );
  return ret;
}

static inline u16 inw(u16 port)
{
  u16 ret;
  asm volatile ( "inw %1, %0"
		 : "=a"(ret)
		 : "Nd"(port) );
  return ret;
}

static inline u32 inl(u16 port)
{
  u32 ret;
  asm volatile ( "inl %1, %0"
		 : "=a"(ret)
		 : "Nd"(port) );
  return ret;
}

static inline void outsb(u16 port, void *buffer, u32 count)
{
  asm volatile (
		"cld;"
		"repne; outsb;"
		: "=D" (buffer), "=c" (count)
		: "d" (port), "0" (buffer), "1" (count)
		: "memory", "cc");
}

static inline void outsw(u16 port, void *buffer, u32 count)
{
  asm volatile (
		"cld;"
		"repne; outsw;"
		: "=D" (buffer), "=c" (count)
		: "d" (port), "0" (buffer), "1" (count)
		: "memory", "cc");
}

static inline void outsl(u16 port, void *buffer, u32 count)
{
  asm volatile (
		"cld;"
		"repne; outsl;"
		: "=D" (buffer), "=c" (count)
		: "d" (port), "0" (buffer), "1" (count)
		: "memory", "cc");
}

static inline void insb(u16 port, void *buffer, u32 count)
{
  asm volatile (
		"cld;"
		"repne; insb;"
		: "=D" (buffer), "=c" (count)
		: "d" (port), "0" (buffer), "1" (count)
		: "memory", "cc");
}

static inline void insw(u16 port, void *buffer, u32 count)
{
  asm volatile (
		"cld;"
		"repne; insw;"
		: "=D" (buffer), "=c" (count)
		: "d" (port), "0" (buffer), "1" (count)
		: "memory", "cc");
}

static inline void insl(u16 port, void *buffer, u32 count)
{
  asm volatile (
		"cld;"
		"repne; insl;"
		: "=D" (buffer), "=c" (count)
		: "d" (port), "0" (buffer), "1" (count)
		: "memory", "cc");
}

void init_serial();
int kwrite(const char *buf, size_t count);
int kprint(const char *buf, ...);

#endif /* !IO_H_ */
