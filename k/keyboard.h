#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#include <k/types.h>

#define BUFFER_DEFAULT 255

void send_command();
void interpret_byte();
extern void init_keymap();

extern char keymap[256];

void init_key_buffer();
void push_key(u8 keycode);
u8 pop_key();

extern u16 push_index;
extern u16 pop_index;
extern u8 key_buffer[256];

#endif /* !KEYBOARD_H_ */
