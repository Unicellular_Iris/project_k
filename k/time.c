#include <k/types.h>
#include <stdint.h>
#include "time.h"
#include "io.h"

u32 g_cycle_whole;
u32 g_cycle_frac;

u32 g_timer_whole;
u32 g_timer_frac;

u32 g_sleep_timer;

void new_cycle()
{
  /* Called once per IRQ 0 */
  
  g_timer_whole += g_cycle_whole;
  g_timer_frac += g_cycle_frac;
  if (g_timer_frac >= 1000000)
    {
      g_timer_frac -= 1000000;
      g_timer_whole++;
    }

  if (g_sleep_timer > 0)
    g_sleep_timer--;

  //  kprint("sleep timer: %d\n\r", g_sleep_timer);
}

void init_timer(uint64_t ms_per_cycle)
{
  u32 ms_temp = (u32)ms_per_cycle;

  g_cycle_whole = (u32)(ms_per_cycle >> 32);
  g_cycle_frac = (uint64_t)ms_temp * 1000000 / ((uint64_t)1 << 32);

  g_timer_whole = 0;
  g_timer_frac = 0;

  kprint("%d.%06d milliseconds between each IRQ 0\n\r", g_cycle_whole, g_cycle_frac);
}

void sleep(u32 value)
{
  g_sleep_timer = value;

  asm volatile("cli");
  while (g_sleep_timer > 0)
      {
	asm volatile("sti;nop;nop;nop;nop;cli");
	kprint("\0"); // For some reason the loop only goes on if I do this
      }
  asm volatile("sti");
}

void print_time()
{
  kprint("Time since init: %d.%d ms\n\r", g_timer_whole, g_timer_frac);
}
