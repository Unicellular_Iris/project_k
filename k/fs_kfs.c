#include <k/kstd.h>
#include <k/kfs.h>
#include "../libs/libk/include/stdlib.h"
#include "multiboot.h"
#include "io.h"
#include "fs_kfs.h"

struct kfs_superblock *kfs_sb;

t_opened_file kfs_files[KFS_MAX_FD];

t_available_file kfs_available_files[KFS_MAX_FD];

u32 kfs_file_count = 0;

u8 *kfs_get_block(int index)
{  
  return (u8 *)((int)kfs_sb + (index * (int)KFS_BLK_SZ));
}

int kfs_get_block_index(struct kfs_inode *inode, off_t offset)
{
  if (offset > inode->file_sz)
    {
//krpint("Offset larger than file\n\rOffset: %d\n\rFile size: %d\n\r", offset, inode->file_sz);
      return (-1);
    }

  u32 block_num = offset / KFS_BLK_SZ;
  
  if (block_num < inode->d_blk_cnt)
    {
      return (inode->d_blks[block_num]);
    }
  else
    {
      u32 iblock_num = (block_num - inode->d_blk_cnt) / (inode->i_blk_cnt * KFS_INDIRECT_BLK_CNT);
      
      struct kfs_iblock *iblock = kfs_get_block(inode->i_blks[iblock_num]);

      return (iblock->blks[(block_num - inode->d_blk_cnt) % ((iblock_num + 1) * KFS_INDIRECT_BLK_CNT)]);
    }
//krpint("Block not found\n\r");
  return (-1);
}

struct kfs_inode *kfs_get_inode_index(int index)
{
  struct kfs_inode *inode = kfs_get_block(kfs_sb->inode_idx);

  for (int i = 0; i < kfs_sb->inode_cnt; i++)
    {
      if (inode->idx == index)
	return (inode);
    
      inode = kfs_get_block(inode->next_inode);
    }
  
  return (NULL);
}

void init_kfs(multiboot_info_t *info)
{
  struct kfs_superblock *sb = (void *)((module_t*)info->mods_addr)[0].mod_start;

  if (sb->magic == KFS_MAGIC)
    {
      if (sb->cksum == kfs_checksum(sb, sizeof(struct kfs_superblock) - sizeof(u32)))
	{
//krpint("\n\r/* Rom pretty print */\n\r\n\r");
	  
	  kfs_sb = sb;
	  kfs_pp_superblock(sb);

	  struct kfs_inode *in;

	  kfs_file_count = sb->inode_cnt;
	  
	  for (int i = 0; i < sb->inode_cnt; i++)
	    {
	      in = kfs_get_inode_index(i + 1);
	      kfs_pp_inode(in);

	      for (int j = 0; j < KFS_FNAME_SZ; j++)
		kfs_available_files[i].filename[j] = in->filename[j];
	      kfs_available_files[i].size = in->file_sz;
	    }

	  for (int i = 0; i < KFS_MAX_FD; i++)
	    {
	      memset(kfs_files[i], 0, sizeof(t_opened_file));
	    }
	}
      //else
	//{
//krpint("KFS superblock checksum invalid!\n\r");
	//}
    }
}

struct kfs_inode *kfs_get_inode(const char *path)
{
  struct kfs_inode *inode = kfs_get_block(kfs_sb->inode_idx);

  for (int i = 0; i < kfs_sb->inode_cnt; i++)
    {
      if (!strncmp(inode->filename, path, KFS_FNAME_SZ))
	return (inode);
    
      inode = kfs_get_block(inode->next_inode);
    }
  
  return (NULL);
}

int kfs_grab_fd(struct kfs_inode *inode)
{
  int i = 0;
 
  while (i < KFS_MAX_FD)
    {
      u8 *ptr = &kfs_files[i];

      if (!*ptr)
	break;

      i++;
    }

  if (i >= KFS_MAX_FD)
    return (-1);

  t_opened_file file;

  file.inode = inode;
  file.offset = 0;
  file.block = kfs_get_block_index(inode, 0);

  kfs_files[i] = file;

  return(i);
}

int kfs_open(const char *path, int flags) // Flags ignored for now
{
  struct kfs_inode *inode = kfs_get_inode(path);

  if (!inode)
    return (-1);

//krpint("Inode found: %d\n\r", inode->idx);
  
  return (kfs_grab_fd(inode));
}

off_t kfs_seek(int fd, off_t offset, int whence)
{
  t_opened_file *file = &kfs_files[fd];

  if (!file) // Wrong fd
    {
//krpint("Invalid fd\n\r");
      return (-1);
    }

  struct kfs_inode *inode = file->inode;
  off_t new_offset;
  u32 new_block;
  
  if (whence == SEEK_SET)
    new_offset = offset;
  else if (whence == SEEK_CUR)
    new_offset = file->offset + offset;
  else if (whence == SEEK_END) // Not going to work for now
    inode->file_sz + offset;

//krpint("New offset: %d\n\r", new_offset);

  new_block = kfs_get_block_index(inode, new_offset);
  
  if (new_block == -1) // Invalid offset
    {
//krpint("Invalid offset\n\r");
      return (-1);
    }

  file->block = new_block;
  file->offset = new_offset;

  return (file->offset);
}

int kfs_read(int fd, void *buf, size_t count)
{
  t_opened_file *file = &kfs_files[fd];

  if (!file) // Wrong fd
    return (-1);

  struct kfs_inode *inode = file->inode;
  int i = 0;
  
  while (i < count && file->offset < inode->file_sz)
    {
//krpint("Offset: %d\n\r", file->offset);
//krpint("Block: %d\n\r", file->block);
      u8 *read_ptr = kfs_get_block(file->block) + file->offset % KFS_BLK_SZ + 12;
      ((u8 *)buf)[i] = *read_ptr;
      if (kfs_seek(fd, 1, SEEK_CUR) == -1)
	return (-1);
      i++;
      read_ptr++;
    }
  return (i);
}

int kfs_close(int fd)
{
  memset(&kfs_files[fd], 0, sizeof(t_opened_file));
  return (0);
}

void kfs_pp_superblock(struct kfs_superblock *sb)
{
//krpint("SuperBlock ");
  kwrite(sb->name, KFS_NAME_SZ);
//krpint("\n\r- Magic number: %d\n\r- Creation time: %d\n\r- Block count: %d\n\r- iNode count: %d\n\r- First iNode index: %d\n\r\n\r", sb->magic, sb->ctime, sb->blk_cnt, sb->inode_cnt, sb->inode_idx);
}

void kfs_pp_inode(struct kfs_inode *inode)
{
//krpint("iNode %d: ", inode->inumber);
  kwrite(inode->filename, KFS_FNAME_SZ);
//krpint("\n\r- Size: %d\n\r- Index: %d\n\r- Block count: %d\n\r- Next iNode: %d\n\r- Direct block count: %d\n\r- Indirect block count: %d\n\r\n\r", inode->file_sz, inode->idx, inode->blk_cnt, inode->next_inode, inode->d_blk_cnt, inode->i_blk_cnt);
}

void kfs_pp_block(struct kfs_block *block)
{
//krpint("Block %d\n\r- Block usage: %d\n\r\n\r", block->idx, block->usage);
}

void kfs_pp_iblock(struct kfs_iblock *iblock)
{
//krpint("iBlock %d\n\r- Block count: %d\n\r\n\r", iblock->idx, iblock->blk_cnt);
}
