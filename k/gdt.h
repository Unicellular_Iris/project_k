#ifndef GDT_H_
#define GDT_H_

#define GDT_SIZE 6
#define USER_STACK_TOP 0xBFFFF000

struct tss_entry_struct
{
   u32 prev_tss;   // The previous TSS - if we used hardware task switching this would form a linked list.
   u32 esp0;       // The stack pointer to load when we change to kernel mode.
   u32 ss0;        // The stack segment to load when we change to kernel mode.
   u32 esp1;       // everything below here is unusued now.. 
   u32 ss1;
   u32 esp2;
   u32 ss2;
   u32 cr3;
   u32 eip;
   u32 eflags;
   u32 eax;
   u32 ecx;
   u32 edx;
   u32 ebx;
   u32 esp;
   u32 ebp;
   u32 esi;
   u32 edi;
   u32 es;         
   u32 cs;        
   u32 ss;        
   u32 ds;        
   u32 fs;       
   u32 gs;         
   u32 ldt;      
   u16 trap;
   u16 iomap_base;
} __packed;
 
typedef struct tss_entry_struct tss_entry_t;

struct tss_entry_bits
{
   unsigned int limit_low:16;
   unsigned int base_low : 24;
   unsigned int accessed :1;
   unsigned int read_write :1; //readable for code, writable for data
   unsigned int conforming_expand_down :1; //conforming for code, expand down for data
   unsigned int code :1; //1 for code, 0 for data
   unsigned int always_1 :1; //should be 1 for everything but TSS and LDT
   unsigned int DPL :2; //priviledge level
   unsigned int present :1;
   unsigned int limit_high :4;
   unsigned int available :1;
   unsigned int always_0 :1; //should always be 0
   unsigned int big :1; //32bit opcodes for code, u32 stack for data
   unsigned int gran :1; //1 to use 4k page addressing, 0 for byte addressing
   unsigned int base_high :8;
} __attribute__((packed));

typedef struct __attribute__((__packed__)) s_gdt_info
{
  u16 limit;
  size_t base;
} t_gdt_info;

typedef struct __attribute__((__packed__)) s_gdt
{
  u16 limit1;
  u16 base1;
  u8 base2;
  u8 access;
  u8 granularity;
  u8 base3;
} t_gdt;

t_gdt gdt[GDT_SIZE];
t_gdt_info gdt_info;
tss_entry_t tss_entry;

void add_gdt_entry(size_t index, unsigned long base, unsigned long limit, u8 access, u8 gran)
{
  gdt[index].base1 = (base & 0xFFFF);
  gdt[index].base2 = (base >> 16) & 0xFF;
  gdt[index].base3 = (base >> 24) & 0xFF;

  gdt[index].limit1 = (limit & 0xFFFF);
  gdt[index].granularity = ((limit >> 16) & 0x0F); // limit high and flags

  gdt[index].granularity |= (gran & 0xF0);
  gdt[index].access = access;
}

void add_tss(){
  u32 base = (u32) &tss_entry;
  u32 limit = sizeof(tss_entry);
  struct tss_entry_bits *g = &(gdt[5]);

  g->limit_low=limit&0xFFFF;
  g->base_low=base&0xFFFFFF; //isolate bottom 24 bits
  g->accessed=1; //This indicates it's a TSS and not a LDT. This is a changed meaning
  g->read_write=0; //This indicates if the TSS is busy or not. 0 for not busy
  g->conforming_expand_down=0; //always 0 for TSS
  g->code=1; //For TSS this is 1 for 32bit usage, or 0 for 16bit.
  g->always_1=0; //indicate it is a TSS
  g->DPL=3; //same meaning
  g->present=1; //same meaning
  g->limit_high=(limit&0xF0000)>>16; //isolate top nibble
  g->available=0;
  g->always_0=0; //same thing
  g->big=0; //should leave zero according to manuals. No effect
  g->gran=0; //so that our computed GDT limit is in bytes, not pages
  g->base_high=(base&0xFF000000)>>24; //isolate top byte.

  // Ensure the TSS is initially zero'd.
  memset(&tss_entry, 0, sizeof(tss_entry));

  tss_entry.ss0  = 0x10;  // Set the kernel stack segment.
  tss_entry.iomap_base = sizeof(tss_entry_t);
}

extern void load_gdt();

#endif /* !GDT_H_ */
