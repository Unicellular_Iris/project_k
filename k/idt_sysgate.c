#include <k/kstd.h>
#include "idt.h"
#include "io.h"

typedef struct {
	u32 code;
	u32 function;
} syscall_functions_t;

u32  syscall_sbrk(u32 increment);

const syscall_functions_t g_syscalls[] = {
	{SYSCALL_SBRK, (u32)&syscall_sbrk}
};

int g_size_syscalls = 1;

u32 syscall_gate(u32 code, u32 param1, u32 param2, u32 param3, u32 param4, u32 param5)
{
  //kprint("Received syscall: %d\n\r- Parameter 1: %d\n\r- Parameter 2: %d\n\r- Parameter 3: %d\n\r- Parameter 4: %d\n\r- Parameter 5: %d\n\r", code, param1, param2, param3, param4, param5);
  u32 Cret = (void *)-1;

  kprint("syscall : %d\n\r", code);
  int i = g_size_syscalls;
  while (i != 0) {
	  i--;
	  if (code == g_syscalls[i].code){
	  	return (Cret = ((u32 (*)(u32, u32, u32))g_syscalls[i].function)(param1, param2, param3));
	  }
  }
  while (1) kprint("1");
}
