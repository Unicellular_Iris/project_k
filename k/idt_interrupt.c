#include "io.h"
#include "idt.h"
#include "keyboard.h"
#include "time.h"

void default_interrupt()
{
  kprint("Unsupported interrupt\n\r");
  outb(0xA0, 0x20);
  outb(0x20, 0x20); //EOI
}

void irq_handler_32(void)
{
  //  kwrite("00\n\r", 4);
  new_cycle();
  outb(0x20, 0x20); //EOI
}

void irq_handler_33(void)
{
  u8 keycode = inb(0x60);

  if (!(keycode >> 7))
    push_key(keycode << 1);
  
  outb(0x20, 0x20); //EOI
}

void irq_handler_34(void)
{
  kwrite("02\n\r", 4);
  outb(0x20, 0x20); //EOI
}

void irq_handler_35(void)
{
  kwrite("03\n\r", 4);
  outb(0x20, 0x20); //EOI
}

void irq_handler_36(void)
{
  kwrite("04\n\r", 4);
  outb(0x20, 0x20); //EOI
}

void irq_handler_37(void)
{
  kwrite("05\n\r", 4);
  outb(0x20, 0x20); //EOI
}

void irq_handler_38(void)
{
  kwrite("06\n\r", 4);
  outb(0x20, 0x20); //EOI
}

void irq_handler_39(void)
{
  kwrite("07\n\r", 4);
  outb(0x20, 0x20); //EOI
}

void irq_handler_40(void)
{
  kwrite("08\n\r", 4);
  outb(0xA0, 0x20);
  outb(0x20, 0x20); //EOI
}

void irq_handler_41(void)
{
  kwrite("09\n\r", 4);
  outb(0xA0, 0x20);
  outb(0x20, 0x20); //EOI
}

void irq_handler_42(void)
{
  kwrite("10\n\r", 4);
  outb(0xA0, 0x20);
  outb(0x20, 0x20); //EOI
}

void irq_handler_43(void)
{
  kwrite("11\n\r", 4);
  outb(0xA0, 0x20);
  outb(0x20, 0x20); //EOI
}

void irq_handler_44(void)
{
  kwrite("12\n\r", 4);
  outb(0xA0, 0x20);
  outb(0x20, 0x20); //EOI
}

void irq_handler_45(void)
{
  kwrite("13\n\r", 4);
  outb(0xA0, 0x20);
  outb(0x20, 0x20); //EOI
}

void irq_handler_46(void)
{
  kwrite("14\n\r", 4);
  outb(0xA0, 0x20);
  outb(0x20, 0x20); //EOI
}

void irq_handler_47(void)
{
  kwrite("15\n\r", 4);
  outb(0xA0, 0x20);
  outb(0x20, 0x20); //EOI
}

void fill_interrupt_gates()
{
  for (int i = 0; i < IDT_SIZE; i++)
    {
      interrupt_handlers[i] = &default_interrupt;
    }

  interrupt_handlers[32] = &irq_handler_32;
  interrupt_handlers[33] = &irq_handler_33;
  interrupt_handlers[34] = &irq_handler_34;
  interrupt_handlers[35] = &irq_handler_35;
  interrupt_handlers[36] = &irq_handler_36;
  interrupt_handlers[37] = &irq_handler_37;
  interrupt_handlers[38] = &irq_handler_38;
  interrupt_handlers[39] = &irq_handler_39;
  interrupt_handlers[40] = &irq_handler_40;
  interrupt_handlers[41] = &irq_handler_41;
  interrupt_handlers[42] = &irq_handler_42;
  interrupt_handlers[43] = &irq_handler_43;
  interrupt_handlers[44] = &irq_handler_44;
  interrupt_handlers[45] = &irq_handler_45;
  interrupt_handlers[46] = &irq_handler_46;
  interrupt_handlers[47] = &irq_handler_47;
}
