#include "hw_pit.h"
#include "io.h"
#include "time.h"
#include <k/types.h>
#include <stdint.h>

#define SHIFT_MASK ((1 << 16) - 1)

void init_pit(u8 pit_channel, u8 pit_access, u8 pit_mode, u32 freq)
{
  u8 pit_command;
  u16 divider;
  u32 real_freq;
  /*  u32 ms_temp;
  u32 ms_whole;
  uint64_t ms_frac;*/
  uint64_t ms_per_cycle;
  
  /* Frequency checks */
  
  if (freq <= 18)
    real_freq = 0x10000; // Lowest possible frequency
  else if (freq >= 1193181)
    real_freq = 1;  // Highest possible frequency

  divider = (3579545 / 3) / freq;

  if (((3579545 / 3) % freq) >= (freq / 2))
    divider++;

  real_freq = (3579545 / 3) / divider; // Actual frequency

  if (((3579545 / 3) % divider) >= (divider / 2))
    real_freq++;			  // Actual frequency rounded right
  
  ms_per_cycle = ((uint64_t)divider * 0xDBB3A062) >> 10; // Magic number, equal to 3000 * (2^42) / 3579545

  init_timer(ms_per_cycle);

  pit_command = 0x0 | pit_channel | pit_access | pit_mode;

  asm volatile("cli");
  
  outb(PIT_COMMAND, pit_command);
  outb(PIT_0 + pit_channel, (u8)divider);
  outb(PIT_0 + pit_channel, (u8)(divider >> 16));

  asm volatile("sti");
}
