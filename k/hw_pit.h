#ifndef HW_PIT_H_
#define HW_PIT_H_

/* I/O ports */
#define PIT_0			0x40 // IRQ 0
#define PIT_1			0x41 // Unused
#define PIT_2			0x42 // Speakers
#define PIT_COMMAND		0x43 // Write-only

/* Command masks */
/* bits 6/7 */
#define PIT_CHANNEL_0		0x00
#define PIT_CHANNEL_1		0x40
#define PIT_CHANNEL_2		0x80
#define PIT_READ_BACK		0xC0

/* bits 4/5 */
#define PIT_COUNTER_LATCH	0x00
#define PIT_ACCESS_LOBYTE	0x10
#define PIT_ACCESS_HIBYTE	0x20
#define PIT_ACCESS_LOHI		0x30

/* bits 1/2/3 */
#define PIT_MODE_0		0x00 // Interrupt on terminal count
#define PIT_MODE_1		0x02 // Re-triggerable one-shot
#define PIT_MODE_2		0x04 // Rate generator
#define PIT_MODE_3		0x06 // Square wave generator
#define PIT_MODE_4		0x08 // Software triggered strobe
#define PIT_MODE_5		0x0A // Hardware triggered strobe
#define PIT_MODE_2_BIS		0x0C // Same as MODE_2
#define PIT_MODE_3_BIS		0x0E // Same as MODE_3

/* bit 0 */
#define PIT_BINARY_16		0x00 // 16-bits binary
#define PIT_BCD_4		0x01 // 4-digit BCD

#endif /* HW_PIT_H_ */
