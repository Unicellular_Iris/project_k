#include "io.h"
#include <stdarg.h>
#include <stdio.h>

void init_serial()
{
  outb(PORT_COM1 + 1, 0x00);
  outb(PORT_COM1 + 3, 0x80);
  outb(PORT_COM1 + 0, 0x03);
  outb(PORT_COM1 + 1, 0x00);
  outb(PORT_COM1 + 3, 0x03);
  outb(PORT_COM1 + 2, 0xC7);
  outb(PORT_COM1 + 4, 0x0B);

  kwrite("Serial port COM1 initialized!\n\r", 31);
}

int kwrite(const char *buf, size_t count)
{
  size_t i;

  for (i = 0; i < count; i++)
    {
      while ((inb(PORT_COM1 + 5) & 0x20) == 0); // Wait for data to be sent
      outb(PORT_COM1, buf[i]);
    }
  return (i);
}

int kprint(const char *buf, ...)
{
  char printf_buf[1024];
  va_list args;
  int printed;

  va_start(args, buf);
  printed = vsprintf(printf_buf, buf, args);
  va_end(args);

  kwrite(printf_buf, printed);
  
  return (printed);
}
