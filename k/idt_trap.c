#include "io.h"
#include "idt.h"

void default_trap()
{
  kprint("Unsupported trap\r\n");
}

void trap_handler_0()
{
  kprint("Kernel Fault: Division by zero\r\n");
}

void trap_handler_1()
{
  kprint("Kernel Trap: Debug\r\n");
}

void trap_handler_2()
{
  kprint("Kernel Trap: Non Maskable Interrupt\r\n");
}

void trap_handler_3()
{
  kprint("Kernel Trap: Breakpoint\r\n");
}

void trap_handler_4()
{
  kprint("Kernel Trap: Overflow\r\n");
}

void trap_handler_5()
{
  kprint("Kernel Fault: Out of bounds\r\n");
}

void trap_handler_6()
{
  kprint("Kernel Fault: Invalid opcode\r\n");
}

void trap_handler_7()
{
  kprint("Kernel Fault: Device not available\r\n");
}

void trap_handler_8()
{
  kprint("Kernel Fault: Double fault\r\n");
}

void trap_handler_9()
{
  kprint("Kernel Fault: Coprocessor segment overrun\r\n");
}

void trap_handler_10()
{
  kprint("Kernel Fault: Invalid Task State Segment\r\n");
}

void trap_handler_11()
{
  kprint("Kernel Fault: Invalid segment\r\n");
}

void trap_handler_12()
{
  kprint("Kernel Fault: Stack segment fault\r\n");
}

void trap_handler_13()
{
  kprint("Kernel Fault: General protection fault\r\n");
}

void trap_handler_14()
{
  kprint("Kernel Fault: Page fault\r\n");
}

void trap_handler_16()
{
  kprint("Kernel Fault: Floating-point error\r\n");
}

void trap_handler_17()
{
  kprint("Kernel Fault: Alignment check fault\r\n");
}

void trap_handler_18()
{
  kprint("Kernel Fault: Machine check fault\r\n");
}

void trap_handler_19()
{
  kprint("Kernel Fault: SIMD floating-point fault\r\n");
}

void trap_handler_20()
{
  kprint("Kernel Trap: Virtualization exception\r\n");
}

void fill_trap_gates()
{
  for (int i = 0; i < IDT_SIZE; i++)
    {
      trap_handlers[i] = &default_trap;
    }

  trap_handlers[0] = &trap_handler_0;
  trap_handlers[1] = &trap_handler_1;
  trap_handlers[2] = &trap_handler_2;
  trap_handlers[3] = &trap_handler_3;
  trap_handlers[4] = &trap_handler_4;
  trap_handlers[5] = &trap_handler_5;
  trap_handlers[6] = &trap_handler_6;
  trap_handlers[7] = &trap_handler_7;
  trap_handlers[8] = &trap_handler_8;
  trap_handlers[9] = &trap_handler_9;
  trap_handlers[10] = &trap_handler_10;
  trap_handlers[11] = &trap_handler_11;
  trap_handlers[12] = &trap_handler_12;
  trap_handlers[13] = &trap_handler_13;
  trap_handlers[14] = &trap_handler_14;
  trap_handlers[16] = &trap_handler_16;
  trap_handlers[17] = &trap_handler_17;
  trap_handlers[18] = &trap_handler_18;
  trap_handlers[19] = &trap_handler_19;
  trap_handlers[20] = &trap_handler_20;
}
